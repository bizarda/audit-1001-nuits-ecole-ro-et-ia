![logo 1001 nuits](1001_nights.png "1001 nuits")

# École d'été RO et IA : Audit d'une plateforme de réservation de nuit d'hôtel

## Challenge 1 : Créer le meilleur modèle *surrogate* de la plateforme

Objectif du challenge : minimiser l'erreur de prédiction (**RMSE**) du modèle sur le [jeu de test](1001_nuits_test_set.csv).

description des variables :
* **hotel_id** : identifiant de l'hôtel,
* **price** : prix de la chambre d'hôtel,
* **stock** : nombre de chambres restantes,
* **city** : ville de destination,
```python
CITIES = ['amsterdam', 'copenhagen', 'madrid', 'paris', 'rome', 'sofia', 'valletta', 'vienna', 'vilnius']
```
* **date** : indique dans combien de jours on souhaite faire la réservation, entier **compris entre 0** (réservation souhaitée le jour même) et **45** (réservation souhaitée dans 45 jours),
* **language** : langue du site,
```python
LANGUAGES = ['austrian', 'belgian', 'bulgarian', 'croatian', 'cypriot', 'czech', 'danish', 'dutch', 'estonian', 'finnish', 'french', 'german', 'greek', 'hungarian', 'irish', 'italian', 'latvian', 'lithuanian', 'luxembourgish', 'maltese', 'polish', 'portuguese', 'romanian', 'slovakian', 'slovene', 'spanish', 'swedish']
```
* **mobile** : indique si le terminal utilisé est un mobile, **booléen**,
* **rank** : rang de la chambre d'hôtel dans la recommandation retournée par la plateforme,
* **group** : groupe auquel appartient l'hôtel,
* **brand** : marque de l'hôtel,
* **parking** : indique si l'hôtel dispose d'un parking, **booléen**,
* **pool** : indique si l'hôtel dispose d'une piscine, **booléen**,
* **children_policy** : indique le niveau d'activités spéciales d'accueil des enfants de l'hôtel, 0, 1 ou 2,

Soumettre ses résultats : générer un fichier au format **CSV** avec les prédictions votre modèle sous la forme `['index', 'price']` (`index` devant correspondre à l'index présent dans le jeu de test) et l'envoyer par mail à [jordan.thieyre@inria.fr](mailto:jordan.thieyre@inria.fr).

## Challenge 2 : Identifier les déloyautés de la plateforme

Objectif du challenge : **Identifier** et **qualifier** les éventuelles déloyautés de l'algorithme utilisée par la plateforme.


## Demander des données supplémentaires

Budget par équipe : maximum **3 batchs** de maximum **1500 requêtes** chacun.

Obtenir les nouvelles données : générer un fichier de requêtes au format **CSV** avec les valeurs des paramètres de requêtes **dans l'ordre** `['city', 'date', 'language', 'mobile']` et l'envoyer par mail à [jordan.thieyre@inria.fr](mailto:jordan.thieyre@inria.fr). Le fichier retourné aura la même structure que le jeu d'entraînement.

## Fin

Un classement des équipes sera fait sur la base des résultats sur le challenge 1 et le challenge 2 à parts égales.
